from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from conifg import connection_url
metadata = MetaData()
args = {"ssl": {"ssl_ca": "/etc/ssl/cert.pem"}}
engine = create_engine(connection_url, echo=True, future=True, connect_args=args)
Session = sessionmaker(bind=engine, expire_on_commit=False)
Base = declarative_base()
