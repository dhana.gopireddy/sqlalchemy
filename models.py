import conifg
from uuid import uuid4
from sqlalchemy import Column, String, Text, DateTime
from sqlalchemy_utils import EmailType, UUIDType
from database import Base


class AboutProject(Base):
    __tablename__ = 'aboutProject'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    p_name = Column('pName', String(255), nullable=False)
    p_details = Column('pDetails', String(2000), nullable=False)
    p_details_file = Column('pDetailsFile', String(255))
    image = Column(String(500))
    due_date = Column('dueDate', DateTime, nullable=False)
    assign_to = Column('assignTo', String(255), nullable=False)
    user_email = Column('userEmail', String(255), nullable=False)
    analysis_document = Column('analysisDocument', String(255))
    design_document = Column('designDocument', String(255))
    final_document = Column('finalDocument', String(255))
    company_name = Column('companyName', String(255))
    email = Column(EmailType, nullable=False)
    get_score = Column('getScore', String(255))


class Registration(Base):
    __tablename__ = 'registration'

    pk = Column(UUIDType(binary=False), default=uuid4)
    name = Column(String(255), nullable=False, index=True)
    image = Column(String(500))
    phone_number = Column('phoneNumber', String(10), nullable=False)
    address = Column(Text(255), nullable=False)
    company_name = Column('companyName', String(50))
    role = Column(String(45))
    email_id = Column('emailId', EmailType, primary_key=True, nullable=False)


class Checklist(Base):
    __tablename__ = 'checkList'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)


class Ample(Base):
    __tablename__ = 'ample'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    ample_q1 = Column('ampleQ1', String(255))
    ample_q2 = Column('ampleQ2', String(255))
    ample_q3 = Column('ampleQ3', String(255))
    ample_q4 = Column('ampleQ4', String(255))
    ample_q5 = Column('ampleQ5', String(255))


class Api(Base):
    __tablename__ = 'api'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    api_q1 = Column('apiQ1', String(255))
    api_q2 = Column('apiQ2', String(255))
    api_q3 = Column('apiQ3', String(255))
    api_q4 = Column('apiQ4', String(255))
    api_q5 = Column('apiQ5', String(255))


class Cloud(Base):
    __tablename__ = 'cloud'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    cloud_q1 = Column('cloudQ1', String(255))
    cloud_q2 = Column('cloudQ2', String(255))
    cloud_q3 = Column('cloudQ3', String(255))
    cloud_q4 = Column('cloudQ4', String(255))
    cloud_q5 = Column('cloudQ5', String(255))


class Datasource(Base):
    __tablename__ = 'dataSource'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    ds_q1 = Column('dsQ1', String(255))
    ds_q2 = Column('dsQ2', String(255))
    ds_q3 = Column('dsQ3', String(255))
    ds_q4 = Column('dsQ4', String(255))
    ds_q5 = Column('dsQ5', String(255))


class Design(Base):
    __tablename__ = 'design'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    design_q1 = Column('designQ1', String(255))
    design_q2 = Column('designQ2', String(255))
    design_q3 = Column('designQ3', String(255))
    design_q4 = Column('designQ4', String(255))
    design_q5 = Column('designQ5', String(255))


class ItPolicy(Base):
    __tablename__ = 'itPolicy'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    it_q1 = Column('itQ1', String(255))
    it_q2 = Column('itQ2', String(255))
    it_q3 = Column('itQ3', String(255))
    it_q4 = Column('itQ4', String(255))
    it_q5 = Column('itQ5', String(255))


class Price(Base):
    __tablename__ = 'price'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    price_q1 = Column('priceQ1', String(255))
    price_q2 = Column('priceQ2', String(255))
    price_q3 = Column('priceQ3', String(255))
    price_q4 = Column('priceQ4', String(255))
    price_q5 = Column('priceQ5', String(255))


class ProjectManagement(Base):
    __tablename__ = 'projectManagement'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    pm_q1 = Column('pmQ1', String(255))
    pm_q2 = Column('pmQ2', String(255))
    pm_q3 = Column('pmQ3', String(255))
    pm_q4 = Column('pmQ4', String(255))
    pm_q5 = Column('pmQ5', String(255))


class RiskManagement(Base):
    __tablename__ = 'riskManagement'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    rm_q1 = Column('rmQ1', String(255))
    rm_q2 = Column('rmQ2', String(255))
    rm_q3 = Column('rmQ3', String(255))
    rm_q4 = Column('rmQ4', String(255))
    rm_q5 = Column('rmQ5', String(255))


class Rpa(Base):
    __tablename__ = 'rpa'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    rpa_q1 = Column('rpaQ1', String(255))
    rpa_q2 = Column('rpaQ2', String(255))
    rpa_q3 = Column('rpaQ3', String(255))
    rpa_q4 = Column('rpaQ4', String(255))
    rpa_q5 = Column('rpaQ5', String(255))


class SecurityMechanism(Base):
    __tablename__ = 'securityMechanism'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    sm_q1 = Column('smQ1', String(255))
    sm_q2 = Column('smQ2', String(255))
    sm_q3 = Column('smQ3', String(255))
    sm_q4 = Column('smQ4', String(255))
    sm_q5 = Column('smQ5', String(255))


class SmartData(Base):
    __tablename__ = 'smartData'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    sd_q1 = Column('sdQ1', String(255))
    sd_q2 = Column('sdQ2', String(255))
    sd_q3 = Column('sdQ3', String(255))
    sd_q4 = Column('sdQ4', String(255))
    sd_q5 = Column('sdQ5', String(255))


class Team(Base):
    __tablename__ = 'team'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    team_q1 = Column('teamQ1', String(255))
    team_q2 = Column('teamQ2', String(255))
    team_q3 = Column('teamQ3', String(255))
    team_q4 = Column('teamQ4', String(255))
    team_q5 = Column('teamQ5', String(255))


class Ux(Base):
    __tablename__ = 'ux'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    ux_q1 = Column('uxQ1', String(255))
    ux_q2 = Column('uxQ2', String(255))
    ux_q3 = Column('uxQ3', String(255))
    ux_q4 = Column('uxQ4', String(255))
    ux_q5 = Column('uxQ5', String(255))


class WhiteLabel(Base):
    __tablename__ = 'whiteLabel'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    wl_q1 = Column('wlQ1', String(255))
    wl_q2 = Column('wlQ2', String(255))
    wl_q3 = Column('wlQ3', String(255))
    wl_q4 = Column('wlQ4', String(255))
    wl_q5 = Column('wlQ5', String(255))


class Wysiwyg(Base):
    __tablename__ = 'wysiwyg'

    pk = Column(UUIDType(binary=False), primary_key=True, default=uuid4)
    user_email = Column('userEmail', EmailType, nullable=False)
    name = Column(String(255), nullable=False)
    image = Column(String(500), nullable=False)
    p_name = Column('pName', String(255), nullable=False)
    p_id = Column('pId', String(255), nullable=False)
    w_q1 = Column('wQ1', String(255))
    w_q2 = Column('wQ2', String(255))
    w_q3 = Column('wQ3', String(255))
    w_q4 = Column('wQ4', String(255))
    w_q5 = Column('wQ5', String(255))
